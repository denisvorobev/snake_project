import React from 'react';
import FieldBlock from "./FieldBlock";

const xCoord = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

const FieldRow = ({snakeBlocksPosition, yIndex}) => {
    const activeItems = snakeBlocksPosition.filter(item => {if (item.y === yIndex) return item});

    return <div className="field-row">
        {
            xCoord.map((item, index) => <FieldBlock isActive={activeItems && activeItems.x === index}/>)
        }
    </div>
}

const Field = () => {
    const [snakeBlocksPosition, setSnake] = React.useState([{ x: 10, y: 10 }, { x: 9, y: 10 }]);
    const [headPosition, setHeadPosition] = React.useState({x: 10, y: 10});

    React.useEffect(() => {
        const timer = setInterval(() => {
            setHeadPosition([{ x: snakeBlocksPosition[0].x + 1, y: snakeBlocksPosition[0].y}])
        }, 500)
        return () => {
            timer.clearTimeout();
        }
    }, [snakeBlocksPosition]);

    React.useEffect(() => {
        // setSnake()
    }, [headPosition])

    return <div className="field">
        {
            xCoord.map((item, index) => <FieldRow snakeBlocksPosition={snakeBlocksPosition} yIndex={index}/>)
        }
    </div>
};

export default Field;