import React from 'react';

const FieldBlock = ({ isActive }) => {
    const classNameChange = ({
        ...isActive && { className: "field-block active"}
    })

    return <div className="field-block" {...classNameChange}>

    </div>
}

export default FieldBlock;