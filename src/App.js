import React from 'react';
import logo from './logo.svg';
import './App.css';
import Field from './GameField/Field/Field';
import "./css/styles.css";

function App() {
  return (
    <div className="App">
      <Field/>
    </div>
  );
}

export default App;
